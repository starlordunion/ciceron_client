package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.TransAdapter;
import starlord.ciceron.mvp.structure.TransComplete;
import starlord.ciceron.mvp.structure.TransPending;
import starlord.ciceron.mvp.structure.TransProgress;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransFragment extends BaseSectionsFragment implements View.OnClickListener {

    private Activity activity;

    private Button  btnPending;
    private Button  btnProgress;
    private Button  btnComplete;

    private TransAdapter pendingAdapter;
    private TransAdapter progressAdapter;
    private TransAdapter completeAdapter;

    private static final int STATUS_PENDING = 0;
    private static final int STATUS_INPROGRESS = 1;
    private static final int STATUS_COMPLETED = 2;

    private static final String DEBUG_TAG = "[TransFragment]";


    public static TransFragment newInstance() {
        TransFragment fragment = new TransFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TransFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        if (getArguments() != null) {

        }
        activity = getActivity();

        pendingAdapter  = new TransAdapter(activity, R.layout.item_list_req, TransPending.getList());
        progressAdapter = new TransAdapter(activity, R.layout.item_list_req, TransProgress.getList());
        completeAdapter = new TransAdapter(activity, R.layout.item_list_req, TransComplete.getList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans, container, false);

        btnPending  = (Button) root.findViewById(R.id.tab_pending_trans);
        btnProgress = (Button) root.findViewById(R.id.tab_progress_trans);
        btnComplete = (Button) root.findViewById(R.id.tab_complete_trans);

        btnPending.setSelected(true);

        btnPending.setOnClickListener(this);
        btnProgress.setOnClickListener(this);
        btnComplete.setOnClickListener(this);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(pendingAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        pendingAdapter.notifyDataSetChanged();
        progressAdapter.notifyDataSetChanged();
        completeAdapter.notifyDataSetChanged();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tab_pending_trans:
                switchCurrentTabTo(STATUS_PENDING);
                btnPending.setSelected(true);
                btnProgress.setSelected(false);
                btnComplete.setSelected(false);
                break;

            case R.id.tab_progress_trans:
                switchCurrentTabTo(STATUS_INPROGRESS);
                btnPending.setSelected(false);
                btnProgress.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case R.id.tab_complete_trans:
                switchCurrentTabTo(STATUS_COMPLETED);
                btnPending.setSelected(false);
                btnProgress.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }


    private void switchCurrentTabTo(final int mode) {
        switch(mode) {
            case STATUS_PENDING:
                pendingAdapter.notifyDataSetChanged();
                setListAdapter(pendingAdapter);

                btnPending.setSelected(true);
                btnProgress.setSelected(false);
                btnComplete.setSelected(false);
                break;

            case STATUS_INPROGRESS:
                progressAdapter.notifyDataSetChanged();
                setListAdapter(progressAdapter);

                btnPending.setSelected(false);
                btnProgress.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case STATUS_COMPLETED:
                completeAdapter.notifyDataSetChanged();
                setListAdapter(completeAdapter);

                btnPending.setSelected(false);
                btnProgress.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if(getListAdapter() == pendingAdapter) {
            if (mListener != null) {
                mListener.beginTranslation(pendingAdapter.getItem(position));
            }
        } else if(getListAdapter() == completeAdapter) {
            if (mListener != null) {

            }
        }
    }


}
