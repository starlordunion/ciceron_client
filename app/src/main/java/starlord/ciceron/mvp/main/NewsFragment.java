package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.NewsFeedAdapter;
import starlord.ciceron.mvp.structure.NewsFeedList;
import starlord.ciceron.mvp.structure.Request;
import starlord.ciceron.mvp.structure.TransPending;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class NewsFragment extends BaseSectionsFragment {

    private Activity activity;
    private NewsFeedAdapter newsFeedAdapter;

    private static int TRANSLATE_JOB;
    private static final int TRANSLATE_ADDQUEUE = 1;
    private static final int TRANSLATE_TRANSLATE = 2;
    private static final int TRANSLATE_CANCEL = 3;

    private static final String DEBUG_TAG = "[NewListFragment]";

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        if (getArguments() != null) {

        }
        activity = getActivity();
        newsFeedAdapter = new NewsFeedAdapter(activity, NewsFeedList.getList());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_newsfeed, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(newsFeedAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        newsFeedAdapter.notifyDataSetChanged();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if(newsFeedAdapter != null) {
            final Request request = newsFeedAdapter.getItem(position);
            showDialog(request);
        }

 /*       if(newsFeedAdapter != null) {
            final Request request = newsFeedAdapter.getItem(position);
            newsFeedDialog = new NewsFeedDialog(getActivity(), request);
            newsFeedDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {

                }
            });
            newsFeedDialog.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    Intent intent = new Intent(getActivity(), TranslateActivity.class);
                    if(request != null) {
                        Bundle reqBundle = request.createBundle();
                        intent.putExtras(reqBundle);
                    }
                    startActivityForResult(intent, TRANSLATE_REQUEST);
                }
            });
            newsFeedDialog.show();
        }
*/

    /*    if (mListener != null) {
            mListener.onItemClick(NewsFeedList.getList().get(position));
        }
    */

    }

    private void showDialog(final Request request) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle("Translation")
                .setItems(R.array.translate_work, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int mode) {
                        switch (mode) {
                            case 0:
                                TransPending.addRequest(request);
                                break;

                            case 1:
                                beginTranslation(request);
                                break;

                            case 2:
                                break;

                            default:
                                break;
                        }
                        dialogInterface.dismiss();
                    }
                });

        builder.create().show();

    }


    private void beginTranslation(Request request) {
        if(mListener != null)
            mListener.beginTranslation(request);
    }

}
