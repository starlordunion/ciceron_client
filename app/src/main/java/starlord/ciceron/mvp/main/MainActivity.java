package starlord.ciceron.mvp.main;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import starlord.ciceron.mvp.R;

import starlord.ciceron.mvp.request.ReqEditActivity;
import starlord.ciceron.mvp.request.RequestActivity;
import starlord.ciceron.mvp.structure.Request;
import starlord.ciceron.mvp.structure.SectionsPagerAdapter;
import starlord.ciceron.mvp.translate.TransCheckActivity;
import starlord.ciceron.mvp.translate.TranslateActivity;


public class MainActivity extends BaseActivity implements BaseSectionsFragment.OnInteractionListener {


    private ViewPager              mViewPager;
    private SectionsPagerAdapter   mPagerAdapter;

    private static final String    DEBUG_TAG = "[MainActivity]";

    private static final int       CREATE_REQUEST = 0;
    private static final int       EDIT_REQUEST = 1;
    private static final int       TRANSLATE_REQUEST = 2;
    private static final int       CHECK_TRANSLATION = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(DEBUG_TAG, "onCreate");

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setLogo(null);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mPagerAdapter = new SectionsPagerAdapter(this, mViewPager);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("News Feed")
                        .setIcon(R.drawable.ic_newsfeed),
                         NewsFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("My Orders")
                        .setIcon(R.drawable.ic_order),
                        OrderFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("My Translations")
                        .setIcon(R.drawable.ic_translate),
                        TransFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("Notifications")
                        .setIcon(R.drawable.ic_status),
                        StatusFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("Setting")
                        .setIcon(R.drawable.ic_config),
                ConfigFragment.class, null
        );

        if (savedInstanceState != null) {
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
 //           case R.id.btn_neworder:

 //               break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
    }


    public void beginTranslation(Request request) {
        Intent intent = new Intent(this, TranslateActivity.class);
        if(request != null) {
            Bundle reqBundle = request.createBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, TRANSLATE_REQUEST);
    }

    public void reviewTranslation(Request request) {
        Intent intent = new Intent(this, TransCheckActivity.class);
        if(request != null) {
            Bundle reqBundle = request.createBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, CHECK_TRANSLATION);
    }

    public void beginRequest() {
        Intent intent = new Intent(this, RequestActivity.class);
        startActivityForResult(intent, CREATE_REQUEST);
    }

    public void editRequest(Request request) {
        Intent intent = new Intent(this, ReqEditActivity.class);
        if(request != null) {
            intent.putExtras(request.createBundle());
        }
        startActivityForResult(intent, EDIT_REQUEST);
    }

}
