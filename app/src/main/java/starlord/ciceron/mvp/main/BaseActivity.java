package starlord.ciceron.mvp.main;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import starlord.ciceron.mvp.common.Fonts;

/**
 * Created by JaehongPark on 15. 1. 13..
 */
public class BaseActivity extends Activity {
    protected ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

//        View view = findViewById(android.R.id.content);
        View view = getWindow().getDecorView();
        setGlobalFont(view);
    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(Fonts.create(this).getFontsGotham());
                    }
                    setGlobalFont(v);
                }
            }
        }
    }

    /*
    protected void manageActionBar(int layoutResId){

        View v = LayoutInflater.from(this).inflate(layoutResId, null);

        setGlobalFont(v);
        //assign the view to the actionbar
        actionBar = getActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setCustomView(v);
    }
*/
}
