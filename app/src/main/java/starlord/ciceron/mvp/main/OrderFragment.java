package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.ReqComplete;
import starlord.ciceron.mvp.structure.ReqPending;
import starlord.ciceron.mvp.structure.ReqProgress;
import starlord.ciceron.mvp.structure.Request;
import starlord.ciceron.mvp.structure.RequestAdapter;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class OrderFragment extends BaseSectionsFragment implements View.OnClickListener {

    private Activity activity;

    private Button  btnPending;
    private Button  btnProgress;
    private Button  btnComplete;
    private ImageButton btnNewRequest;

    private RequestAdapter pendingAdapter;
    private RequestAdapter progressAdapter;
    private RequestAdapter completeAdapter;

    private static final int STATUS_PENDING = 0;
    private static final int STATUS_INPROGRESS = 1;
    private static final int STATUS_COMPLETED = 2;

    private static final String DEBUG_TAG = "[OrderFragment]";


    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");


        if (getArguments() != null) {

        }
        activity = getActivity();

        pendingAdapter  = new RequestAdapter(activity, ReqPending.getList());
        progressAdapter = new RequestAdapter(activity, ReqProgress.getList());
        completeAdapter = new RequestAdapter(activity, ReqComplete.getList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_order, container, false);

        btnPending    = (Button) root.findViewById(R.id.tab_pending_order);
        btnProgress   = (Button) root.findViewById(R.id.tab_progress_order);
        btnComplete   = (Button) root.findViewById(R.id.tab_complete_order);
        btnNewRequest = (ImageButton) root.findViewById(R.id.btn_neworder);

        btnPending.setSelected(true);

        btnPending.setOnClickListener(this);
        btnProgress.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
        btnNewRequest.setOnClickListener(this);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(pendingAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        pendingAdapter.notifyDataSetChanged();
        progressAdapter.notifyDataSetChanged();
        completeAdapter.notifyDataSetChanged();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        editRequest((Request) getListAdapter().getItem(position));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tab_pending_order:
                switchCurrentTabTo(STATUS_PENDING);
                break;

            case R.id.tab_progress_order:
                switchCurrentTabTo(STATUS_INPROGRESS);
                break;

            case R.id.tab_complete_order:
                switchCurrentTabTo(STATUS_COMPLETED);
                break;

            case R.id.btn_neworder:
                beginRequest();
                break;
        }
    }


    private void beginRequest() {
        if(mListener != null)
            mListener.beginRequest();
    }

    private void editRequest(final Request request) {
        if (request != null) {
            if (mListener != null)
                mListener.editRequest(request);
        }
    }


    private void switchCurrentTabTo(final int mode) {
        switch(mode) {
            case STATUS_PENDING:
                pendingAdapter.notifyDataSetChanged();
                setListAdapter(pendingAdapter);

                btnPending.setSelected(true);
                btnProgress.setSelected(false);
                btnComplete.setSelected(false);
                break;

            case STATUS_INPROGRESS:
                progressAdapter.notifyDataSetChanged();
                setListAdapter(progressAdapter);

                btnPending.setSelected(false);
                btnProgress.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case STATUS_COMPLETED:
                completeAdapter.notifyDataSetChanged();
                setListAdapter(completeAdapter);

                btnPending.setSelected(false);
                btnProgress.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }



}
