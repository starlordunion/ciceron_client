package starlord.ciceron.mvp.structure;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 4..
 */
public class StatusAdapter extends ArrayAdapter<Status> {
    private Context       context;
    private List<Status>  list;
    private int           resLayout;

    public StatusAdapter(Context context, int resLayout, List<Status> list) {
        super(context, resLayout, list);
        this.context = context;
        this.list = list;
        this.resLayout = resLayout;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Status getItem(int position) {
        return super.getItem(position);
    }

    protected class ViewHolder {
        ImageView img_status;
        TextView  text_time;
        TextView  text_status;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(resLayout, null, false);
            holder = new ViewHolder();
            //holder.img_status = view.findViewById(R.id.);
            //holder.text_time = view.findViewById(R.id.);
            //holder.text_status = view.findViewById(R.id.);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        //TODO:modify this
/*
        holder.img_status.setBackground();
        holder.text_time.setText();
        holder.text_status.setText();
*/
//      view.setOnClickListener(null);
        return view;
    }
}
