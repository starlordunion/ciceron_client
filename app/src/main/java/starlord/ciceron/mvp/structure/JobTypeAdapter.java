package starlord.ciceron.mvp.structure;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.JobType;
import starlord.ciceron.mvp.common.Lanaguage;

/**
 * Created by JaehongPark on 15. 1. 24..
 */
public class JobTypeAdapter extends ArrayAdapter<String> {
    private Activity activity;

    public JobTypeAdapter(Activity activity) {
        super(activity, R.layout.item_list_jobtype, activity.getResources().getStringArray(R.array.job_type));
        this.activity = activity;
    }

    private class ViewHolder {
        TextView lang_txt;
        ImageView lang_img;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        String type = getItem(position);

        if (view == null) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_list_jobtype, null, false);
            holder = new ViewHolder();
            holder.lang_txt = (TextView) view.findViewById(R.id.text_req_type);
            holder.lang_img = (ImageView) view.findViewById(R.id.img_req_type);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.lang_txt.setText(type);
        holder.lang_txt.setTypeface(Fonts.create(activity).getFontsGotham());
        holder.lang_img.setImageResource(JobType.getDrawable(type));

        return view;
    }
}
