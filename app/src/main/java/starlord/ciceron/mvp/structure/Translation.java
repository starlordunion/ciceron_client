package starlord.ciceron.mvp.structure;

import android.os.Bundle;

/**
 * Created by JaehongPark on 15. 1. 10..
 */
public class Translation {
    public String id;
    public String translator;
    public String content;
    public String comment;
    public String transdate;
    public int    words;

    public Translation(String id, String translator, String content,
                       String comment, String transdate, int words) {
        this.id = id;
        this.translator = translator;
        this.content = content;
        this.comment = comment;
        this.transdate = transdate;
        this.words = words;
    }

    public boolean isValid() {
        if(id != null && translator != null && content != null && comment != null)
            return true;
        return false;
    }

    public static Translation makeFromBundle(Bundle bundle) {
        String id        = bundle.getString("id");
        String translator = bundle.getString("translator");
        String content   = bundle.getString("content");
        String comment   = bundle.getString("comment");
        String transdate = bundle.getString("transdate");
        int    words     = bundle.getInt("words");

        Translation translation = new Translation(id, translator, content,
                                                  comment, transdate, words);
        return translation;
    }
}
