package starlord.ciceron.mvp.structure;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Subject;

/**
 * Created by JaehongPark on 15. 1. 17..
 */
public class ReqSubjectAdapter extends ArrayAdapter<String> {
    private Activity activity;

    public ReqSubjectAdapter(Activity activity) {
        super(activity, R.layout.item_grid_req_subject, activity.getResources().getStringArray(R.array.subject));
        this.activity = activity;
    }

    private class ViewHolder {
        TextView subject_txt;
        ImageView subject_img;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_grid_req_subject, null, false);
            holder = new ViewHolder();
            holder.subject_txt = (TextView) view.findViewById(R.id.text_req_subject);
            holder.subject_img = (ImageView) view.findViewById(R.id.img_req_subject);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Point winSize = new Point();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getSize(winSize);
        int cellsize = winSize.x / 3;
        view.setLayoutParams(new AbsListView.LayoutParams(cellsize, cellsize));

        holder.subject_txt.setText(getItem(position).toUpperCase());
        holder.subject_txt.setTypeface(Fonts.create(activity).getFontsGotham());
        holder.subject_img.setImageResource(Subject.getDrawable(position));
        return view;
    }
}
