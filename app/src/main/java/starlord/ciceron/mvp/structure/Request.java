package starlord.ciceron.mvp.structure;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JaehongPark on 14. 11. 6..
 */
public class Request implements Parcelable {
    public String id;
    public String requester;
    public int    format;
    public int    subject;
    public String content;
    public String context;
    public String duedate;
    public int    fromlang;
    public int    tolang;
    public int    words;
    public int    points;
    private Translation translation;

    public static final int TRANS = 10;
    public static final int SIMPL = 11;

    public Request() {
        this.id = null;
        this.requester = null;
        this.format = -1;
        this.subject = -1;
        this.content = null;
        this.context = null;
        this.duedate = null;
        this.fromlang = -1;
        this.tolang = -1;
        this.words = -1;
        this.points = -1;
    }

    public Request(final String id, final String requestor,
                   final int format, final int subject,
                   final String content, final String context, final String duedate,
                   final int fromlang, final int tolang,
                   final int words, final int points) {

        this.id = id;
        this.requester = requestor;
        this.format = format;
        this.subject = subject;
        this.content = content;
        this.context = context;
        this.duedate = duedate;
        this.fromlang = fromlang;
        this.tolang = tolang;
        this.words = words;
        this.points = points;
    }

    public Request(Parcel req) {
        this.id = req.readString();
        this.requester = req.readString();
        this.content = req.readString();
        this.context = req.readString();
        this.subject = req.readInt();
        this.format = req.readInt();
        this.fromlang = req.readInt();
        this.tolang = req.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.requester);
        dest.writeString(this.content);
        dest.writeString(this.context);
        dest.writeInt(this.subject);
        dest.writeInt(this.format);
        dest.writeInt(this.fromlang);
        dest.writeInt(this.tolang);
    }

    public static final Creator<Request> CREATOR = new Creator<Request>() {
        public Request createFromParcel(Parcel in) {
            return new Request(in);
        }

        public Request[] newArray(int size) {
            return new Request[size];
        }
    };

    public Bundle createBundle() {
        if(this.isValid()) {
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            bundle.putString("requester", requester);
            bundle.putInt("format", format);
            bundle.putInt("subject", subject);
            bundle.putString("content", content);
            bundle.putString("context", context);
            bundle.putString("duedate", duedate);
            bundle.putInt("fromlang", fromlang);
            bundle.putInt("tolang", tolang);
            bundle.putInt("words", words);
            bundle.putInt("points", points);

            return bundle;
        }
        return null;
    }

    public static Request makeFromBundle(Bundle bundle) {
        String id        = bundle.getString("id");
        String requester = bundle.getString("requester");
        int    format    = bundle.getInt("format");
        int    subject   = bundle.getInt("subject");
        String content   = bundle.getString("content");
        String context   = bundle.getString("context");
        String duedate   = bundle.getString("duedate");
        int    fromlang  = bundle.getInt("fromlang");
        int    tolang    = bundle.getInt("tolang");
        int    words     = bundle.getInt("words");
        int    points    = bundle.getInt("points");

        Request request = new Request(id, requester, format, subject,
                                      content, context, duedate,
                                      fromlang, tolang, words, points);
        return request;
    }

    public boolean isValid() {
        return (id != null) && (requester != null) && (content != null) &&
               (fromlang >=0) && (tolang >= 0);
    }

    public static int getRequestType(Bundle bundle) {

        String reqCode = bundle.getString("id").substring(0, 5);
        if (reqCode.equals("TRANS"))
            return TRANS;
        else if (reqCode.equals("SIMPL"))
            return SIMPL;
        else
            return -1;
    }

    public boolean hasTranslation() {
        if(translation != null)
            return true;
        return false;
    }

    public void addTranslation(Translation translation) {
        if(translation != null)
            if(translation.isValid())
                this.translation = translation;
    }

    @Override
    public String toString() {
        return null;
    }
}
