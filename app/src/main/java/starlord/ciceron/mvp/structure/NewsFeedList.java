package starlord.ciceron.mvp.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsFeedList {
    private static List<Request> req_list = new ArrayList<Request>();
    private static Map<String, Request> req_map = new HashMap<String, Request>();

    static {
        // Add 3 sample items.
 /*       addRequest(new Request("aaaaa", "Ashley", 0, 2, "저는 예전부터 음악을 즐겼습니다. 심심할 때나 기분이 안 좋을 때 음악을 들었고 어느 순간 내가 음악을 내가 직접 만들면 어떨까 하는 생각이 문득 들었습니다. 주위에 작곡 하는 형 누나들을 만나서 얘기도 해보고 부모님께도 음악을 하고 싶다는 의사를 표현했습니다. 하지만 아무도 제가 좋은 선택을 했다고 말해주지 않았고 저는 절망했습니다. 그렇지만 포기하지 않고 끝까지 달린 결과, 저는 여기까지 왔고, 앞으로도 포기하지 않을 거라는 굳은 믿음을가지고 있습니다.",
                "어학연수를 가기 위한 지원서에 나의 꿈과 그 꿈을 가지게 된 계기에 대해 물어보기에 적은 답변입니다. 학교에 내야할 지원서인만큼 회화영어보다는 좀 더 아카데믹한 영어로 번역해 주셨으면 좋겠습니다."
                , "31/12/14 11:59pm", 0, 1, 218, 3000));

        addRequest(new Request("aaaaa", "Ashley", 0, 2, "저는 예전부터 음악을 즐겼습니다. 심심할 때나 기분이 안 좋을 때 음악을 들었고 어느 순간 내가 음악을 내가 직접 만들면 어떨까 하는 생각이 문득 들었습니다. 주위에 작곡 하는 형 누나들을 만나서 얘기도 해보고 부모님께도 음악을 하고 싶다는 의사를 표현했습니다. 하지만 아무도 제가 좋은 선택을 했다고 말해주지 않았고 저는 절망했습니다. 그렇지만 포기하지 않고 끝까지 달린 결과, 저는 여기까지 왔고, 앞으로도 포기하지 않을 거라는 굳은 믿음을가지고 있습니다.",
                "어학연수를 가기 위한 지원서에 나의 꿈과 그 꿈을 가지게 된 계기에 대해 물어보기에 적은 답변입니다. 학교에 내야할 지원서인만큼 회화영어보다는 좀 더 아카데믹한 영어로 번역해 주셨으면 좋겠습니다."
                , "31/12/14 11:59pm", 0, 1, 218, 3000));

        addRequest(new Request("bbbbb", "Ashley", 0, 2, "저는 예전부터 음악을 즐겼습니다. 심심할 때나 기분이 안 좋을 때 음악을 들었고 어느 순간 내가 음악을 내가 직접 만들면 어떨까 하는 생각이 문득 들었습니다. 주위에 작곡 하는 형 누나들을 만나서 얘기도 해보고 부모님께도 음악을 하고 싶다는 의사를 표현했습니다. 하지만 아무도 제가 좋은 선택을 했다고 말해주지 않았고 저는 절망했습니다. 그렇지만 포기하지 않고 끝까지 달린 결과, 저는 여기까지 왔고, 앞으로도 포기하지 않을 거라는 굳은 믿음을가지고 있습니다.",
                "어학연수를 가기 위한 지원서에 나의 꿈과 그 꿈을 가지게 된 계기에 대해 물어보기에 적은 답변입니다. 학교에 내야할 지원서인만큼 회화영어보다는 좀 더 아카데믹한 영어로 번역해 주셨으면 좋겠습니다."
                , "31/12/14 11:59pm", 0, 1, 218, 3000));

        addRequest(new Request("ccccc", "Ashley", 0, 2, "저는 예전부터 음악을 즐겼습니다. 심심할 때나 기분이 안 좋을 때 음악을 들었고 어느 순간 내가 음악을 내가 직접 만들면 어떨까 하는 생각이 문득 들었습니다. 주위에 작곡 하는 형 누나들을 만나서 얘기도 해보고 부모님께도 음악을 하고 싶다는 의사를 표현했습니다. 하지만 아무도 제가 좋은 선택을 했다고 말해주지 않았고 저는 절망했습니다. 그렇지만 포기하지 않고 끝까지 달린 결과, 저는 여기까지 왔고, 앞으로도 포기하지 않을 거라는 굳은 믿음을가지고 있습니다.",
                "어학연수를 가기 위한 지원서에 나의 꿈과 그 꿈을 가지게 된 계기에 대해 물어보기에 적은 답변입니다. 학교에 내야할 지원서인만큼 회화영어보다는 좀 더 아카데믹한 영어로 번역해 주셨으면 좋겠습니다."
                , "31/12/14 11:59pm", 0, 1, 218, 3000));

        addRequest(new Request("ddddd", "Ashley", 0, 2, "저는 예전부터 음악을 즐겼습니다. 심심할 때나 기분이 안 좋을 때 음악을 들었고 어느 순간 내가 음악을 내가 직접 만들면 어떨까 하는 생각이 문득 들었습니다. 주위에 작곡 하는 형 누나들을 만나서 얘기도 해보고 부모님께도 음악을 하고 싶다는 의사를 표현했습니다. 하지만 아무도 제가 좋은 선택을 했다고 말해주지 않았고 저는 절망했습니다. 그렇지만 포기하지 않고 끝까지 달린 결과, 저는 여기까지 왔고, 앞으로도 포기하지 않을 거라는 굳은 믿음을가지고 있습니다.",
                "어학연수를 가기 위한 지원서에 나의 꿈과 그 꿈을 가지게 된 계기에 대해 물어보기에 적은 답변입니다. 학교에 내야할 지원서인만큼 회화영어보다는 좀 더 아카데믹한 영어로 번역해 주셨으면 좋겠습니다."
                , "31/12/14 11:59pm", 0, 1, 218, 3000));

*/
    }

    public static void addRequest(Request request) {
        /* if the list does not have any requests with same id,
           add request to the list */
        if(request != null) {
            String id = request.id;
            if(req_map.containsKey(id)) {
                req_list.remove(req_map.get(id));
                req_map.remove(id);
            }
            req_list.add(request);
            req_map.put(id, request);
        }
    }

    public static void removeRequest(String id) {
        if(req_map.containsKey(id)) {
            req_list.remove(req_map.get(id));
            req_map.remove(id);
        }
    }
    public static List<Request> getList() {
        if(req_list != null)
            return req_list;
        return null;
    }
}
