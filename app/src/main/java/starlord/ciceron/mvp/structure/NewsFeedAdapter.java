package starlord.ciceron.mvp.structure;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Format;
import starlord.ciceron.mvp.common.Lanaguage;
import starlord.ciceron.mvp.common.Subject;

/**
 * Created by JaehongPark on 14. 12. 28..
 */
public class NewsFeedAdapter extends ArrayAdapter<Request> {
    private Context context;
    private List<Request> list;

    public NewsFeedAdapter(Context context, List<Request> list) {
        super(context, R.layout.item_list_newsfeed1, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Request getItem(int position) {
        return super.getItem(position);
    }

    protected class ViewHolder {
        ImageView picture;
        TextView name;

        ImageView img_format;
        ImageView img_subject;
        TextView  text_format;
        TextView  text_subject;

        ImageView img_fromlang;
        ImageView img_tolang;
        TextView  text_fromlang;
        TextView  text_tolang;

        TextView duedate;
        TextView words;
        TextView words2;
        TextView price;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        Request request = list.get(position);

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_list_newsfeed1, null, false);
            holder = new ViewHolder();
            holder.picture = (ImageView) view.findViewById(R.id.img_profile_news);
            holder.name = (TextView) view.findViewById(R.id.text_name_news);

            holder.img_format = (ImageView) view.findViewById(R.id.img_format_news);
            holder.img_subject = (ImageView) view.findViewById(R.id.img_subject_news);
            holder.text_format = (TextView) view.findViewById(R.id.text_format_news);
            holder.text_subject = (TextView) view.findViewById(R.id.text_subject_news);

            holder.img_fromlang = (ImageView) view.findViewById(R.id.img_fromlang_news);
            holder.img_tolang = (ImageView) view.findViewById(R.id.img_tolang_news);
            holder.text_fromlang = (TextView) view.findViewById(R.id.text_fromlang_news);
            holder.text_tolang = (TextView) view.findViewById(R.id.text_tolang_news);

            holder.duedate = (TextView) view.findViewById(R.id.text_duedate_news);
            holder.words = (TextView) view.findViewById(R.id.text_words_news);
            holder.words2 = (TextView) view.findViewById(R.id.text_words2_news);
            holder.price = (TextView) view.findViewById(R.id.text_money_news);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        int format = request.format;
        int subject = request.subject;
        int fromlang = request.fromlang;
        int tolang = request.tolang;
        StringBuffer words = new StringBuffer(String.valueOf(request.words));
        StringBuffer price = new StringBuffer(String.valueOf(request.points));

        holder.name.setText(request.requester);

        holder.img_format.setImageResource(Format.getDrawable(format));
        holder.img_subject.setImageResource(Subject.getDrawable(subject));
        holder.text_format.setText(Format.getTitle(format));
        holder.text_format.setTypeface(Fonts.create(context).getFontsGotham());
        holder.text_subject.setText(Subject.getTitle(subject));
        holder.text_subject.setTypeface(Fonts.create(context).getFontsGotham());

        holder.img_fromlang.setImageResource(Lanaguage.getDrawable(fromlang));
        holder.img_tolang.setImageResource(Lanaguage.getDrawable(tolang));
        holder.text_fromlang.setText(Lanaguage.getTitle(fromlang));
        holder.text_fromlang.setTypeface(Fonts.create(context).getFontsGotham());
        holder.text_tolang.setText(Lanaguage.getTitle(tolang));
        holder.text_tolang.setTypeface(Fonts.create(context).getFontsGotham());

        holder.duedate.setText(request.duedate);
        holder.duedate.setTypeface(Fonts.create(context).getFontsGotham());
        holder.words2.setText(String.valueOf(words.toString()));
        holder.words2.setTypeface(Fonts.create(context).getFontsGotham());
        holder.words.setText(words.append(" words").toString());
        holder.words.setTypeface(Fonts.create(context).getFontsGotham());
//        holder.price.setText(String.valueOf(request.points));
        holder.price.setText(price.append(" $").toString());
        holder.price.setTypeface(Fonts.create(context).getFontsGotham());

        return view;
    }
}
