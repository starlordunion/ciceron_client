package starlord.ciceron.mvp.structure;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Format;

/**
 * Created by JaehongPark on 15. 1. 17..
 */
public class ReqFormatAdapter extends ArrayAdapter<String> {
    private Activity activity;

    public ReqFormatAdapter(Activity activity) {
        super(activity, R.layout.item_grid_req_format, activity.getResources().getStringArray(R.array.format));
        this.activity = activity;
    }

    private class ViewHolder {
        TextView format_txt;
        ImageView format_img;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_grid_req_format, null, false);
            holder = new ViewHolder();
            holder.format_txt = (TextView)  view.findViewById(R.id.text_req_format);
            holder.format_img = (ImageView) view.findViewById(R.id.img_req_format);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Point winSize = new Point();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getSize(winSize);
        int cellsize = winSize.x / 3;
        view.setLayoutParams(new AbsListView.LayoutParams(cellsize, cellsize));

        holder.format_txt.setText(getItem(position).toUpperCase());
        holder.format_txt.setTypeface(Fonts.create(activity).getFontsGotham());
        holder.format_img.setImageResource(Format.getDrawable(position));
        return view;
    }
}
