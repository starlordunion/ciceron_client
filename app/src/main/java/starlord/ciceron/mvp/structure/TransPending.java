package starlord.ciceron.mvp.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by JaehongPark on 14. 12. 26..
 */
public class TransPending {
    private static List<Request> trans_list = new ArrayList<Request>();
    private static Map<String, Request> trans_map = new HashMap<String, Request>();

    public static void addRequest(Request request) {
        /* if the list does not have any requests with same id,
           add request to the list */
        if(request != null) {
            String id = request.id;
            if(trans_map.containsKey(id)) {
                trans_list.remove(trans_map.get(id));
                trans_map.remove(id);
            }
            trans_list.add(request);
            trans_map.put(id, request);
        }
    }

    public static void removeRequest(String id) {
        if(trans_map.containsKey(id)) {
            trans_list.remove(trans_map.get(id));
            trans_map.remove(id);
        }
    }
    public static List<Request> getList() {
        if(trans_list != null)
            return trans_list;
        return null;
    }
}
