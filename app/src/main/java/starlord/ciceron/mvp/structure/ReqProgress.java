package starlord.ciceron.mvp.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReqProgress {
    private static List<Request> req_list = new ArrayList<Request>();
    private static Map<String, Request> req_map = new HashMap<String, Request>();

    public static void addRequest(Request request) {
        /* if the list does not have any requests with same id,
           add request to the list */
        if(request != null) {
            String id = request.id;
            if(req_map.containsKey(id)) {
                req_list.remove(req_map.get(id));
                req_map.remove(id);
            }
            req_list.add(request);
            req_map.put(id, request);
        }
    }

    public static void removeRequest(String id) {
        if(req_map.containsKey(id)) {
            req_list.remove(req_map.get(id));
            req_map.remove(id);
        }
    }
    public static List<Request> getList() {
        if(req_list != null)
            return req_list;
        return null;
    }
}
