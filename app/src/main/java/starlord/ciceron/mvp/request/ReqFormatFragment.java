package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.ReqFormatAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqFormatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqFormatFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqFormatFragment extends BaseRequestFragment implements AdapterView.OnItemClickListener {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqContentFragment.
     */
    private Activity activity;
    private GridView grid_format;
    private int      reqformat = -1;

    private static final String DEBUG_TAG = "[ReqFormatFragment]";

    public static ReqFormatFragment newInstance(int stageNum) {
        ReqFormatFragment fragment = new ReqFormatFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqFormatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_format, container, false);

        grid_format = (GridView) root.findViewById(R.id.grid_req_format);
        grid_format.setAdapter(new ReqFormatAdapter(getActivity()));
        grid_format.setOnItemClickListener(this);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        reqformat = position;
        view.setSelected(true);
        invalidateOptionsMenu();
    }

    @Override
    protected boolean isReadyToGo() {
        if(reqformat >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.submitInteger("format", reqformat);
        }
    }

}
