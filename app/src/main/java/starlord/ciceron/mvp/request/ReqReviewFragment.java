package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqReviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqReviewFragment extends BaseRequestFragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqReviewFragment.
     */
    private Bundle    reqBundle;
    private RequestOverview requestView;
    private Button    btnComplete;

    private static final String DEBUG_TAG = "[ReqReviewFragment]";

    public static ReqReviewFragment newInstance(int stageNum) {
        ReqReviewFragment fragment = new ReqReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_req_review, container, false);

        requestView = (RequestOverview) root.findViewById(R.id.view_request_req_review);
        requestView.setLayout(reqBundle);

        btnComplete   = (Button) root.findViewById(R.id.btn_complete_req_review);
        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitRequestInfo();
            }
        });

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    //    setLayout();
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_request, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menuNext.setVisible(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    public void setReqBundle(Bundle reqBundle) {
        this.reqBundle = reqBundle;
    }

    @Override
    protected boolean isReadyToGo() {
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.onComplete(reqBundle);
        }
    }
}
