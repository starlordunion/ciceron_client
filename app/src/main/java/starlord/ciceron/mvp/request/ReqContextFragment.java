package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqContextFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqContextFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqContextFragment extends BaseRequestFragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqInfoFragment.
     */

    private EditText edit_context;

    private static final String DEBUG_TAG = "[ReqContextFragment]";

    public static ReqContextFragment newInstance(int stageNum) {
        ReqContextFragment fragment = new ReqContextFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqContextFragment() {
        super();
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_context, container, false);

        edit_context = (EditText) root.findViewById(R.id.edit_req_context);
        edit_context.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edit_context.requestFocus();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        edit_context.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                manager.showSoftInput(edit_context, 0);
            }
        }, 100);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onResume");

        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(edit_context.getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected boolean isReadyToGo() {
        if(edit_context != null) {
            if(edit_context.length() > 0)
                return true;
        }
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(edit_context != null) {
            if(mListener != null) {
                String context = edit_context.getText().toString();
                mListener.submitString("context", context);
                //TODO:MODIFY THIS:
                mListener.submitString("duedate", "12/31/14 11:59");
            }
        }
    }
}
