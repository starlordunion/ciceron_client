package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.ReqSubjectAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqSubjectFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqSubjectFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqSubjectFragment extends BaseRequestFragment implements AdapterView.OnItemClickListener {

    /*
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment ReqInfoFragment.
         */
    private GridView  grid_subject;
    private int       reqsubject = -1;

    private static final String DEBUG_TAG = "[ReqSubjectFragment]";

    public static ReqSubjectFragment newInstance(int stageNum) {
        ReqSubjectFragment fragment = new ReqSubjectFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqSubjectFragment() {
        super();
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_subject, container, false);
        grid_subject = (GridView) root.findViewById(R.id.grid_req_subject);
        grid_subject.setAdapter(new ReqSubjectAdapter(getActivity()));
        grid_subject.setOnItemClickListener(this);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");

        if(savedInstanceState != null) {
            reqsubject = savedInstanceState.getInt("subject");
            grid_subject.setSelection(reqsubject);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState");
        if(reqsubject >= 0)
            outState.putInt("subject", reqsubject);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        reqsubject = position;
        view.setSelected(true);
        invalidateOptionsMenu();
    }

    protected boolean isReadyToGo() {
        if(reqsubject >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.initRequest();
            mListener.submitInteger("subject", reqsubject);
        }
    }
}
