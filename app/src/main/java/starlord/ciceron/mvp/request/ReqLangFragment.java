package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Lanaguage;
import starlord.ciceron.mvp.structure.JobTypeAdapter;
import starlord.ciceron.mvp.structure.LanguageAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqLangFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqLangFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqLangFragment extends BaseRequestFragment implements OnClickListener {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqInfoFragment.
     */

    private ImageView img_fromlang;
    private ImageView img_tolang;
    private ImageView img_switch;

    private TextView  text_fromlang;

    private TextView  text_tolang;

    //TODO: 언어 매핑 후 -1 로 바꿔야
    private int         fromlang = -1;
    private int         tolang = -1;
    private int         whichlang;

    private LanguageAdapter langAdapter;
    private JobTypeAdapter  typeAdapter;

    private static final String DEBUG_TAG = "[ReqLangFragment]";

    public static ReqLangFragment newInstance(int stageNum) {
        ReqLangFragment fragment = new ReqLangFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqLangFragment() {
        super();
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        langAdapter = new LanguageAdapter(getActivity());
        typeAdapter = new JobTypeAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_req_lang, container, false);
        img_fromlang = (ImageView) root.findViewById(R.id.btn_fromlang_lang);
        img_tolang = (ImageView) root.findViewById(R.id.btn_tolang_lang);
        img_switch = (ImageView) root.findViewById(R.id.btn_switchlang_lang);

        img_fromlang.setOnClickListener(this);
        img_tolang.setOnClickListener(this);
        img_switch.setOnClickListener(this);

        text_fromlang = (TextView) root.findViewById(R.id.text_fromlang_lang);
        text_tolang = (TextView) root.findViewById(R.id.text_tolang_lang);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                moveBackToPrevious();
                break;

            case R.id.btn_request_nextstep:
                showTypeDialog();
//                moveOnToNext();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_fromlang_lang:
                whichlang = R.id.btn_fromlang_lang;
                break;

            case R.id.btn_tolang_lang:
                whichlang = R.id.btn_tolang_lang;
                break;

            case R.id.btn_switchlang_lang:
                switchLanguage();
                return;
        }
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle("Choose Language")
                .setAdapter(langAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        selectLanguage(position);
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
        dialog.show();
    }

    private void showTypeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle("Choose Type")
                .setAdapter(typeAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
        dialog.show();
    }

    private void selectLanguage(int which) {
        if(whichlang == R.id.btn_fromlang_lang) {
            selectFromLanguage(which);
        } else if(whichlang == R.id.btn_tolang_lang) {
            selectToLanguage(which);
        }
    }

    private void selectFromLanguage(int lang) {
        fromlang = lang;
        text_fromlang.setText(langAdapter.getItem(lang));
        img_fromlang.setImageResource(Lanaguage.getDrawable(lang));
    }

    private void selectToLanguage(int lang) {
        tolang = lang;
        text_tolang.setText(langAdapter.getItem(lang));
        img_tolang.setImageResource(Lanaguage.getDrawable(lang));
    }

    private void switchLanguage() {
        if(isReadyToGo()) {
            int temp = fromlang;
            fromlang = tolang;
            tolang = temp;

            selectFromLanguage(fromlang);
            selectToLanguage(tolang);
        }
    }

    @Override
    protected boolean isReadyToGo() {
        if(fromlang >= 0 && tolang >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.submitInteger("fromlang", fromlang);
            mListener.submitInteger("tolang", tolang);
        }
    }
}
