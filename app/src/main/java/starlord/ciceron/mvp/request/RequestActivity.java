package starlord.ciceron.mvp.request;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Timer;
import starlord.ciceron.mvp.structure.NewsFeedList;
import starlord.ciceron.mvp.structure.ReqPending;
import starlord.ciceron.mvp.structure.Request;

public class RequestActivity extends BaseActivity implements BaseRequestFragment.OnRequestInteractionListener {

    private FragmentManager         fm;
    private FragmentTransaction     ft;
    private ProgressBar             pb;
    private Bundle                  reqBundle;

    private ReqSubjectFragment      reqSubjectFrag;
    private ReqFormatFragment       reqFormatFrag;
    private ReqLangFragment         reqLangFrag;
    private ReqTextFragment         reqTextFrag;
    private ReqContextFragment      reqContextFrag;
    private ReqReviewFragment       reqReviewFrag;

    private static final CharSequence[] stageTitle = {
                                                 "Choose Subject",
                                                 "Set Format",
                                                 "Set Language",
                                                 "Type in Text",
                                                 "Context",
                                                 "Review Order" };

    private static final String    DEBUG_TAG = "[RequestActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        Log.d(DEBUG_TAG, "onCreate");

        initRequestActivity();

        if(savedInstanceState == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_content_request, reqSubjectFrag);
            ft.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    private void initRequestActivity() {
        fm = getFragmentManager();
        ft = fm.beginTransaction();
        pb = (ProgressBar) findViewById(R.id.pb_request);

        reqSubjectFrag = ReqSubjectFragment.newInstance(0);
        reqFormatFrag = ReqFormatFragment.newInstance(1);
        reqLangFrag = ReqLangFragment.newInstance(2);
        reqTextFrag = ReqTextFragment.newInstance(3);
        reqContextFrag = ReqContextFragment.newInstance(4);
        reqReviewFrag = ReqReviewFragment.newInstance(5);

    }

    public void initRequest() {
        reqBundle = new Bundle();

        //TODO: modify this
        reqBundle.putString("id", String.valueOf(Timer.newInstance()));
        reqBundle.putString("requester", "Jaehong");
    }

    public void onStage(int step) {
        pb.setProgress(step * 20);
        actionBar.setTitle(stageTitle[step]);
    }

    public void moveOn() {
        Fragment fragment = fm.findFragmentById(R.id.frame_content_request);

        if(fragment != null) {

            if (fragment instanceof ReqSubjectFragment) {
                reqSubjectFrag.submitRequestInfo();
                fragment = reqFormatFrag;
            } else if (fragment instanceof ReqFormatFragment) {
                reqFormatFrag.submitRequestInfo();
                fragment = reqLangFrag;
            } else if (fragment instanceof ReqLangFragment) {
                reqLangFrag.submitRequestInfo();
                fragment = reqTextFrag;
            } else if (fragment instanceof ReqTextFragment) {
                reqTextFrag.submitRequestInfo();
                fragment = reqContextFrag;
            } else if (fragment instanceof ReqContextFragment) {
                reqContextFrag.submitRequestInfo();
                reqReviewFrag.setReqBundle(reqBundle);
                fragment = reqReviewFrag;
            } else {
                // Do nothing
            }

            ft = fm.beginTransaction();
            ft.replace(R.id.frame_content_request, fragment);
            ft.commit();
        }
    }

    public void moveBack() {
        Fragment fragment = fm.findFragmentById(R.id.frame_content_request);

        if(fragment != null) {
            if (fragment instanceof ReqSubjectFragment) {
                onBackPressed();
                return;
            } else if (fragment instanceof ReqFormatFragment) {
                fragment = reqSubjectFrag;
            } else if (fragment instanceof ReqLangFragment) {
                fragment = reqFormatFrag;
            } else if (fragment instanceof ReqTextFragment) {
                fragment = reqLangFrag;
            } else if (fragment instanceof ReqContextFragment) {
                fragment = reqTextFrag;
            } else {
                fragment = reqContextFrag;
            }

            ft = fm.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.replace(R.id.frame_content_request, fragment);
            ft.commit();
        }
    }

    public void submitString(String key, String value) {
        if(reqBundle != null)
            reqBundle.putString(key, value);
    }


    public void submitInteger(String key, int value) {
        if(reqBundle != null)
            reqBundle.putInt(key, value);
    }

    public void onComplete(Bundle bundle) {
        Request request = Request.makeFromBundle(bundle);
        if(request.isValid()) {
            ReqPending.addRequest(request);
            NewsFeedList.addRequest(request);
            Toast.makeText(getBaseContext(), "Request Complete", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showDialog() {

    }
}
