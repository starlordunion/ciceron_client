package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseRequestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BaseRequestFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public abstract class BaseRequestFragment extends Fragment {

    protected int stageNum = 0;
    protected MenuItem menuNext;
    protected OnRequestInteractionListener mListener;
    protected static Typeface font;

    public BaseRequestFragment() { }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnRequestInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnRequestInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mListener != null) {
            mListener.onStage(stageNum);
        }
        setHasOptionsMenu(true);
        font = Fonts.create(getActivity()).getFontsGotham();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        View view = getView().findViewById(android.R.id.content);
        View view = getActivity().getWindow().getDecorView();
        setGlobalFont(view);
    }

    @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_request, menu);
        menuNext = menu.findItem(R.id.btn_request_nextstep);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                moveBackToPrevious();
                break;

            case R.id.btn_request_nextstep:
                moveOnToNext();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected void setStageNum(final int stageNum) {
        this.stageNum = stageNum;
    }

    protected int getStageNo() {
        return stageNum;
    }

    protected void moveBackToPrevious() {
        if(mListener != null)
            mListener.moveBack();
    }

    protected void moveOnToNext() {
        if(mListener != null)
            mListener.moveOn();
    }

    protected void invalidateOptionsMenu() {
        if(isReadyToGo()) {
            if (menuNext != null)
                menuNext.setEnabled(true);
        }
        else {
            if (menuNext != null)
                menuNext.setEnabled(false);
        }
    }


    abstract protected boolean isReadyToGo();
    abstract protected void submitRequestInfo();

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnRequestInteractionListener {
        public void initRequest();
        public void onStage(int step);
        public void moveOn();
        public void moveBack();
        public void submitString(String key, String value);
        public void submitInteger(String key, int value);
        public void onComplete(Bundle reqBundle);

    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(font);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }
}
