package starlord.ciceron.mvp.request;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqTextFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqTextFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqTextFragment extends BaseRequestFragment implements View.OnClickListener {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqContentFragment.
     */
    private TextView txt_typemessage;

    private TextView txt_textsize;
    private EditText edit_content;

    private ScrollView scroll_photo;

    private Button btn_text;
    private Button btn_photo;
    private Button btn_audio;
    private Button btn_file;

    private static final int TYPE_TEXT = 0;
    private static final int TYPE_PHOTO = 1;
    private static final int TYPE_AUDIO = 2;
    private static final int TYPE_FILE = 3;

    private static final String DEBUG_TAG = "[ReqTextFragment]";

    public static ReqTextFragment newInstance(int stageNum) {
        ReqTextFragment fragment = new ReqTextFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqTextFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_text, container, false);

        btn_text  = (Button) root.findViewById(R.id.btn_typetext_content);
        btn_text.setOnClickListener(this);
        btn_photo = (Button) root.findViewById(R.id.btn_typephoto_content);
        btn_photo.setOnClickListener(this);
        btn_audio = (Button) root.findViewById(R.id.btn_typeaudio_content);
        btn_audio.setOnClickListener(this);
        btn_file  = (Button) root.findViewById(R.id.btn_typefile_content);
        btn_file.setOnClickListener(this);

        scroll_photo = (ScrollView) root.findViewById(R.id.scroll_req_content);

        txt_textsize = (TextView) root.findViewById(R.id.txt_req_textsize);
        edit_content = (EditText) root.findViewById(R.id.edit_req_text);
        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                txt_textsize.setText(charSequence.length() + " words");
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edit_content.setTypeface(BaseRequestFragment.font);
        edit_content.requestFocus();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");

        switchCurrentTabTo(TYPE_TEXT);
        edit_content.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                manager.showSoftInput(edit_content, 0);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");

        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(edit_content.getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_typetext_content:
                switchCurrentTabTo(TYPE_TEXT);
                break;

            case R.id.btn_typephoto_content:
                switchCurrentTabTo(TYPE_PHOTO);
                break;

            case R.id.btn_typeaudio_content:
                switchCurrentTabTo(TYPE_AUDIO);
                break;

            case R.id.btn_typefile_content:
                switchCurrentTabTo(TYPE_FILE);
                break;
        }
    }

    private void switchCurrentTabTo(final int type) {
        switch(type) {
            case TYPE_TEXT:
                btn_text.setSelected(true);
                btn_photo.setSelected(false);
                btn_audio.setSelected(false);
                btn_file.setSelected(false);
                showTextTypeInfo();
                break;

            case TYPE_PHOTO:
                btn_text.setSelected(false);
                btn_photo.setSelected(true);
                btn_audio.setSelected(false);
                btn_file.setSelected(false);
                showImageTypeInfo();
                break;

            case TYPE_AUDIO:
                btn_text.setSelected(false);
                btn_photo.setSelected(false);
                btn_audio.setSelected(true);
                btn_file.setSelected(false);
                showAudioTypeInfo();
                break;

            case TYPE_FILE:
                btn_text.setSelected(false);
                btn_photo.setSelected(false);
                btn_audio.setSelected(false);
                btn_file.setSelected(true);
                showFileTypeInfo();
                break;
        }
    }

    private void showTextTypeInfo() {
        showEditText();
        hideImage();
        hideAudio();
    }

    private void showImageTypeInfo() {
        hideEditText();
        showImage();
        hideAudio();
    }

    private void showAudioTypeInfo() {
        hideEditText();
        hideImage();
        showAudio();
    }

    private void showFileTypeInfo() {
        hideEditText();
        hideImage();
        hideAudio();
    }

    private void showEditText() {
        if (txt_textsize != null && edit_content != null) {
            txt_textsize.setVisibility(View.VISIBLE);
            edit_content.setVisibility(View.VISIBLE);
        }
    }

    private void hideEditText() {
        if (txt_textsize != null && edit_content != null) {
            txt_textsize.setVisibility(View.INVISIBLE);
            edit_content.setVisibility(View.INVISIBLE);
        }
    }

    private void showImage() {
        if(scroll_photo != null)
            scroll_photo.setVisibility(View.VISIBLE);
    }

    private void hideImage() {
        if(scroll_photo != null)
            scroll_photo.setVisibility(View.INVISIBLE);
    }

    private void showAudio() {

    }

    private void hideAudio() {

    }

    private void showFile() {

    }

    private void hideFile() {

    }

    @Override
    protected boolean isReadyToGo() {
        if(edit_content != null) {
            if(edit_content.length() > 0)
                return true;
        }
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(edit_content != null) {
            String text = edit_content.getText().toString();
            if(mListener != null) {
                mListener.submitString("content", text);
                mListener.submitInteger("words", text.length());

                //TODO: Modify this
                mListener.submitInteger("points", text.length() * 100);
            }
        }
    }
/*
    private void setKeyboardVisibility(final boolean visible) {
        if (visible) {
            if(inputMethodManager != null)

                inputMethodManager.showSoftInput(edit_content,0);
        } else {
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(edit_content.getWindowToken(), 0);
            }
        }
    }
    */
}
