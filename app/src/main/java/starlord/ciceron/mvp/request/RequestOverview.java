package starlord.ciceron.mvp.request;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Format;
import starlord.ciceron.mvp.common.Lanaguage;
import starlord.ciceron.mvp.common.Subject;

/**
 * Created by JaehongPark on 15. 1. 10..
 */
public class RequestOverview extends RelativeLayout {

    private Bundle    reqBundle;

    private ImageView img_fromlang;
    private ImageView img_tolang;
    private ImageView img_format;
    private ImageView img_subject;

    private TextView  text_duedate;
    private TextView  text_content;
    private TextView  text_context;
    private TextView  text_fromlang;
    private TextView  text_tolang;
    private TextView  text_format;
    private TextView  text_subject;
    private TextView  text_words;
    private TextView  text_points;

    public RequestOverview(Context context) {
        super(context);
        initLayout();
    }

    public RequestOverview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }

    public RequestOverview(Context context, Bundle reqBundle) {
        super(context);
        initLayout();
    }

    public RequestOverview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout();
    }

    public void initLayout() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_req_overview, this, true);

        img_fromlang = (ImageView) findViewById(R.id.img_fromlang);
        img_tolang = (ImageView) findViewById(R.id.img_tolang);
        img_format = (ImageView) findViewById(R.id.img_format);
        img_subject = (ImageView) findViewById(R.id.img_subject);

        text_duedate = (TextView) findViewById(R.id.text_duedate);
        text_content = (TextView) findViewById(R.id.text_content_req);
        text_context = (TextView) findViewById(R.id.text_context);
        text_fromlang = (TextView) findViewById(R.id.text_fromlang);
        text_tolang = (TextView) findViewById(R.id.text_tolang);
        text_format = (TextView) findViewById(R.id.text_format);
        text_subject = (TextView) findViewById(R.id.text_subject);
        text_words = (TextView) findViewById(R.id.text_words);
        text_points = (TextView) findViewById(R.id.text_points);
    }

    public void setLayout(Bundle reqBundle) {

        if(reqBundle != null) {
            Integer fromlang = reqBundle.getInt("fromlang");
            Integer tolang = reqBundle.getInt("tolang");
            Integer format = reqBundle.getInt("format");
            Integer subject = reqBundle.getInt("subject");
            Integer words = reqBundle.getInt("words");
            Integer points = reqBundle.getInt("points");

            String duedate = reqBundle.getString("duedate");
            String content = reqBundle.getString("content");
            String context = reqBundle.getString("context");

            img_fromlang.setImageResource(Lanaguage.getDrawable(fromlang));
            img_tolang.setImageResource(Lanaguage.getDrawable(tolang));

            img_format.setImageResource(Format.getDrawable(format));
            img_subject.setImageResource(Subject.getDrawable(subject));

            text_fromlang.setText(Lanaguage.getTitle(fromlang));
            text_tolang.setText(Lanaguage.getTitle(tolang));

            text_format.setText(Format.getTitle(format));
            text_subject.setText(Subject.getTitle(subject));

            text_duedate.setText(duedate.toString());
            text_words.setText(words.toString());
            text_points.setText(points.toString());

            text_content.setText(content);
            text_context.setText(context);
        }
    }

}
