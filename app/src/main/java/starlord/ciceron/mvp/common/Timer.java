package starlord.ciceron.mvp.common;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by JaehongPark on 15. 1. 13..
 */
public class Timer {
    private static Random random;

    public static int newInstance() {
        if(random == null) {
            random = new Random();
            return random.nextInt();
        }
        return random.nextInt();
    }
}
