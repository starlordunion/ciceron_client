package starlord.ciceron.mvp.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by JaehongPark on 14. 11. 23..
 */
public class Fonts {
    private Context context;
    private static Fonts instance;
    private static Typeface font_gotham;
    private static Typeface font_zeronero;

    public Fonts(Context context) {
        this.context = context;
    }

    public static Fonts create(Context context) {
        if (instance == null) {
            instance = new Fonts(context);
            font_gotham = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/gotham_medium.otf");
            font_zeronero = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/zeronero.ttf");
        }
        return instance;
    }

    public Typeface getFontsGotham() {
        return font_gotham;
    }

    public Typeface getFontsZeronero() {
        return font_zeronero;
    }

/*    public Typeface getTypeFace(String fonts) {
        return font_gotham;
    }

    */
}
