package starlord.ciceron.mvp.common;

import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Lanaguage {
    private static HashMap<Integer, String> lang_map_string = new HashMap<Integer, String>();
    private static HashMap<Integer, Integer> lang_map_drawable = new HashMap<Integer, Integer>();

    static {
        lang_map_string.put(0, "Korean");
        lang_map_string.put(1, "English(USA)");

        lang_map_drawable.put(0, R.drawable.ic_korean);
        lang_map_drawable.put(1, R.drawable.ic_usa2);
    }

    public static Integer getDrawable(Integer lang) {
        if(lang_map_drawable.containsKey(lang))
            return lang_map_drawable.get(lang);
        return null;
    }

    public static Integer getDrawable(String lang) {
        return null;
    }

    public static String getTitle(int lang) {
        if(lang_map_string.containsKey(lang))
            return lang_map_string.get(lang);
        return null;
    }
}
