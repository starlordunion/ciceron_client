package starlord.ciceron.mvp.common;

import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Format {
    private static HashMap<Integer, String> format_string = new HashMap<Integer, String>();
    private static HashMap<Integer, Integer> format_drawable = new HashMap<Integer, Integer>();

    static {
        format_string.put(0, "Email");
        format_string.put(1, "Letter");
        format_string.put(2, "Essay");
        format_string.put(3, "Inquiry");
        format_string.put(4, "News");
        format_string.put(5, "Menu");
        format_string.put(6, "Invitation");
        format_string.put(7, "Report");
        format_string.put(8, "Research");
        format_string.put(9, "Script");
        format_string.put(10, "Homework");
        format_string.put(11, "Application");

        format_drawable.put(0, R.drawable.ic_email);
        format_drawable.put(1, R.drawable.ic_letter);
        format_drawable.put(2, R.drawable.ic_essay);
        format_drawable.put(3, R.drawable.ic_inquiry);
        format_drawable.put(4, R.drawable.ic_news);
        format_drawable.put(5, R.drawable.ic_menu);
        format_drawable.put(6, R.drawable.ic_invitation);
        format_drawable.put(7, R.drawable.ic_report);
        format_drawable.put(8, R.drawable.ic_research);
        format_drawable.put(9, R.drawable.ic_script);
        format_drawable.put(10, R.drawable.ic_homework);
        format_drawable.put(11, R.drawable.ic_application);
    }

    public static Integer getDrawable(Integer format) {
        if(format_drawable.containsKey(format))
            return format_drawable.get(format);
        return null;
    }

    public static String getTitle(int format) {
        if(format_string.containsKey(format))
            return format_string.get(format);
        return null;
    }
}
