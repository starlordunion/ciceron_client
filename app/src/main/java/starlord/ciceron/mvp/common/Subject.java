package starlord.ciceron.mvp.common;

import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Subject {
    private static HashMap<Integer, String> subject_string = new HashMap<Integer, String>();
    private static HashMap<Integer, Integer> subject_drawable = new HashMap<Integer, Integer>();

    static {
        subject_string.put(0, "History");
        subject_string.put(1, "Technology");
        subject_string.put(2, "Art\nDesign");
        subject_string.put(3, "Social\nScience");
        subject_string.put(4, "Religion");
        subject_string.put(5, "Literature");
        subject_string.put(6, "Medical");
        subject_string.put(7, "Culture");
        subject_string.put(8, "Politics");
        subject_string.put(9, "Sports");
        subject_string.put(10, "Business");
        subject_string.put(11, "Other");



        subject_drawable.put(0, R.drawable.ic_history);
        subject_drawable.put(1, R.drawable.ic_technology);
        subject_drawable.put(2, R.drawable.ic_design);
        subject_drawable.put(3, R.drawable.ic_social);
        subject_drawable.put(4, R.drawable.ic_religion);
        subject_drawable.put(5, R.drawable.ic_literature);
        subject_drawable.put(6, R.drawable.ic_medical);
        subject_drawable.put(7, R.drawable.ic_culture);
        subject_drawable.put(8, R.drawable.ic_politics);
        subject_drawable.put(9, R.drawable.ic_sports);
        subject_drawable.put(10, R.drawable.ic_business);
        subject_drawable.put(11, R.drawable.ic_other);
    }

    public static Integer getDrawable(Integer subject) {
        if(subject_drawable.containsKey(subject)) {
            return subject_drawable.get(subject);
        }
        return null;
    }

    public static String getTitle(int subject) {
        if(subject_string.containsKey(subject))
            return subject_string.get(subject);
        return null;
    }
}
