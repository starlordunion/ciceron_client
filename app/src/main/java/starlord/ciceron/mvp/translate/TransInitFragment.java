package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.request.RequestOverview;
import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransInitFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransInitFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransInitFragment extends BaseTransFragment {

    private ImageView img_requestor;
    private TextView  text_requestor;

    private RequestOverview requestView;

    private static final String DEBUG_TAG = "[TransInitFragment]";

    public static TransInitFragment newInstance(int stageNum, Bundle reqBundle) {
        TransInitFragment fragment = new TransInitFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNum);
        bundle.putBundle("reqBundle", reqBundle);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public TransInitFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
            reqBundle = getArguments().getBundle("reqBundle");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans_init, container, false);

        img_requestor = (ImageView) root.findViewById(R.id.img_profile_trans_init);
        text_requestor = (TextView) root.findViewById(R.id.text_profile_trans_init);
        text_requestor.setText(reqBundle.getString("requester"));

        requestView = (RequestOverview) root.findViewById(R.id.view_request_trans_init);
        requestView.setLayout(reqBundle);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void submitTransInfo() {

    }
}
