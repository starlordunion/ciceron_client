package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseTransFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BaseTransFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public abstract class BaseTransFragment extends Fragment {

    protected Bundle reqBundle;
    protected int stageNum = 0;
    protected MenuItem menuNext;
    protected OnTransInteractionListener mListener;
    private static Typeface font;

    public BaseTransFragment() { }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnTransInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTransInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mListener != null) {
            mListener.onStage(stageNum);
        }
        setHasOptionsMenu(true);
        font = Fonts.create(getActivity()).getFontsGotham();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getActivity().getWindow().getDecorView();
        setGlobalFont(view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_translate, menu);
        menuNext = menu.findItem(R.id.btn_translate_nextstep);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                if(mListener != null)
                    mListener.moveBack();
                break;

            case R.id.btn_translate_nextstep:
                if(mListener != null)
                    mListener.moveOn();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected void setStageNum(int stageNum) {
        this.stageNum = stageNum;
    }

    protected int getStageNum() {
        return stageNum;
    }

    abstract protected void submitTransInfo();

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTransInteractionListener {
        public void onStage(int step);
        public void moveOn();
        public void moveBack();
        public void submitInteger(String key, int value);
        public void submitString(String key, String value);
        public void onComplete();

    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(font);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }
}
