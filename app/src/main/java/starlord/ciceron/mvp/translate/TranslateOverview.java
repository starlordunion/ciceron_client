package starlord.ciceron.mvp.translate;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 11..
 */
public class TranslateOverview extends RelativeLayout {
    private Bundle transBundle;

    private TextView  text_transdate;
    private TextView  text_content;
    private TextView  text_comment;

    public TranslateOverview(Context context) {
        super(context);
        initLayout();
    }

    public TranslateOverview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }

    public TranslateOverview(Context context, Bundle transBundle) {
        super(context);
        initLayout();
    }

    public TranslateOverview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout();
    }

    public void initLayout() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_trans_overview, this, true);

        text_transdate = (TextView) findViewById(R.id.text_transdate);
        text_content = (TextView) findViewById(R.id.text_content_trans);
        text_comment = (TextView) findViewById(R.id.text_comment);

    }

    public void setLayout(Bundle transBundle) {

        if(transBundle != null) {
            String transdate = transBundle.getString("transdate");
            String content = transBundle.getString("content");
            String comment = transBundle.getString("comment");

            text_transdate.setText(transdate);
            text_content.setText(content);
            text_comment.setText(comment);
        }
    }
}
