package starlord.ciceron.mvp.translate;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.structure.Request;
import starlord.ciceron.mvp.structure.TransComplete;
import starlord.ciceron.mvp.structure.Translation;

public class TranslateActivity extends BaseActivity implements BaseTransFragment.OnTransInteractionListener {
    private FragmentManager fm;
    private FragmentTransaction ft;
    private ProgressBar pb;

    private Bundle      reqBundle;
    private Bundle      transBundle;

    private TransInitFragment       transInitFrag;
    private TransMainFragment       transMainFrag;
    private TransCommentFragment    transCommentFrag;
    private TransReviewFragment     transReviewFrag;

    private static final CharSequence[] stageTitle = {
            "Original Post",
            "Type in Translation",
            "Comment",
            "Review Translation" };

    private static final String    DEBUG_TAG = "[TranslateActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate);
        Log.d(DEBUG_TAG, "onCreate");

        initTranslateActivity();

        if(savedInstanceState == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_content_translate, transInitFrag);
            ft.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    /*    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_request, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            switch(id) {
                case android.R.id.home:
                    moveBackToPrevStage();
                    break;

                case R.id.btn_request_nextstep:
                    moveOnToNextStage();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    */

    public void initTranslateActivity() {
        if(getIntent().getExtras() != null)
            reqBundle = getIntent().getExtras();

        fm = getFragmentManager();
        ft = fm.beginTransaction();
        pb = (ProgressBar) findViewById(R.id.pb_translate);

        transBundle = new Bundle();
        //TODO: modify this
        transBundle.putString("id", "aaaaa");
        transBundle.putString("translator", "bbbbb");

        transInitFrag = TransInitFragment.newInstance(1, reqBundle);
        transMainFrag = TransMainFragment.newInstance(2, reqBundle);
        transCommentFrag = TransCommentFragment.newInstance(3);
        transReviewFrag = TransReviewFragment.newInstance(4, reqBundle, transBundle);
    }

    public void onStage(int step) {
        pb.setProgress(step * 25);
        actionBar.setTitle(stageTitle[step-1]);
    }

    public void moveOn() {
        Fragment fragment = fm.findFragmentById(R.id.frame_content_translate);

        if(fragment != null) {

            if (fragment instanceof TransInitFragment) {
                transInitFrag.submitTransInfo();
                fragment = transMainFrag;
            } else if (fragment instanceof TransMainFragment) {
                transMainFrag.submitTransInfo();
                fragment = transCommentFrag;
            } else if (fragment instanceof TransCommentFragment) {
                transCommentFrag.submitTransInfo();
                fragment = transReviewFrag;
            } else if (fragment instanceof TransReviewFragment) {
                // Do nothing
            } else {

            }

            ft = fm.beginTransaction();
            ft.replace(R.id.frame_content_translate, fragment);
            ft.commit();
        }
    }

    public void moveBack() {
        Fragment fragment = fm.findFragmentById(R.id.frame_content_translate);

        if(fragment != null) {
            if (fragment instanceof TransInitFragment) {
                onBackPressed();
                return;
            } else if (fragment instanceof TransMainFragment) {
                fragment = transInitFrag;
            } else if (fragment instanceof TransCommentFragment) {
                fragment = transMainFrag;
            } else if (fragment instanceof TransReviewFragment) {
                fragment = transCommentFrag;
            } else {

            }

            ft = fm.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.replace(R.id.frame_content_translate, fragment);
            ft.commit();
        }
    }

    public void submitInteger(String key, int value) {
        if(transBundle != null)
            transBundle.putInt(key, value);
    }

    public void submitString(String key, String value) {
            //    Toast.makeText(getBaseContext(), "getString", Toast.LENGTH_SHORT).show();
        if(transBundle != null)
            transBundle.putString(key, value);
    }

    public void onComplete() {
        Translation translation = Translation.makeFromBundle(transBundle);
        if(reqBundle != null) {
            Request request = Request.makeFromBundle(reqBundle);
            request.addTranslation(translation);
            TransComplete.addRequest(request);
        }
        finish();
    }
}
