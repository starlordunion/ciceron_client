package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.request.RequestOverview;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransReviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransReviewFragment extends BaseTransFragment {
    private Bundle transBundle;
    private TranslateOverview translateView;
    private RequestOverview   requestView;
    private Button            btnSubmit;

    private static final String DEBUG_TAG = "[TransReviewFragment]";

    public static TransReviewFragment newInstance(int stageNo, Bundle reqBundle, Bundle transBundle) {
        TransReviewFragment fragment = new TransReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("stageNum", stageNo);
        bundle.putBundle("reqBundle", reqBundle);
        bundle.putBundle("transBundle", transBundle);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public TransReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            stageNum = getArguments().getInt("stageNum");
            reqBundle = getArguments().getBundle("reqBundle");
            transBundle = getArguments().getBundle("transBundle");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans_check, container, false);

        translateView = (TranslateOverview) root.findViewById(R.id.view_translate_trans_review);
        translateView.setLayout(transBundle);

        requestView = (RequestOverview) root.findViewById(R.id.view_request_trans_review);
        requestView.setLayout(reqBundle);

        btnSubmit = (Button) root.findViewById(R.id.btn_submit_trans_review);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitTransInfo();
            }
        });
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_request, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menuNext.setVisible(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void submitTransInfo() {
        if(mListener != null)
            mListener.onComplete();
    }
}
